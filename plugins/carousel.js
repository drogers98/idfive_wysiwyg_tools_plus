// Inserts shortcode for custom block view for FAES. Relies on the insert_block module.
(function ($) {
	Drupal.wysiwyg.plugins.carousel = {
	  invoke: function(data,settings,instanceId) {
 			Drupal.wysiwyg.instances[instanceId].insert('[block:idfive=idfive-carousel]');
		},
	}
}(jQuery));
