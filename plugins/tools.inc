<?php
// This is where we define the actual tools that will be added.
function idfive_tools_plugin() {
	return array(
			// Add custom carousel shortcode
			'carousel' => array(
				'path' => drupal_get_path('module', 'idfive_wysiwyg_tools_plus') . '/plugins',
				// Define what js file (function) you want
				'js file' => 'carousel.js',
				// Define JS Path
				'js path' => drupal_get_path('module', 'idfive_wysiwyg_tools_plus') . '/plugins',
				// Define CSS Path
				'css path' => drupal_get_path('module', 'idfive_wysiwyg_tools_plus') . '/plugins',
				// Define the in-editor css
				'css file' => 'wysiwyg_view.css',
				// Define Hover State Icon Title
				'icon title' => 'Slideshow',
				// Define icon path
	      'icon path' => drupal_get_path('module', 'idfive_wysiwyg_tools_plus') . '/plugins/icons',
				// Define icon you want
	      'icon file' => 'slideshow.png',
				// Define the Button Name
				'buttons' => array('carousel' => t('carousel')),
				// See API if you want to modify the following junx...
				'load' => TRUE,
				'internal' => FALSE,
				'settings' => array(),
				),

			// Add custom code for two column layout
			'two_columns' => array(
				'path' => drupal_get_path('module', 'idfive_wysiwyg_tools_plus') . '/plugins',
				'js file' => 'two_columns.js',
				'js path' => drupal_get_path('module', 'idfive_wysiwyg_tools_plus') . '/plugins',
				'css path' => drupal_get_path('module', 'idfive_wysiwyg_tools_plus') . '/plugins',
				'css file' => 'wysiwyg_view.css',
				'icon title' => 'Two Column Layout',
	      'icon path' => drupal_get_path('module', 'idfive_wysiwyg_tools_plus') . '/plugins/icons',
	      'icon file' => 'twoColumn.png',
				'buttons' => array('two_columns' => t('two_columns')),
				'load' => TRUE,
				'internal' => FALSE,
				'settings' => array(),
				),

	);
}
