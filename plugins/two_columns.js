// Function to add two column layout markup
(function ($) {
	Drupal.wysiwyg.plugins.two_columns = {
		invoke: function(data,settings,instanceId) {
	 		Drupal.wysiwyg.instances[instanceId].insert('<div class="twoColumn"> <p>ADD TWO COLUMN TEXT CONTENT HERE</p> </div>');
		},
	}
}(jQuery));
