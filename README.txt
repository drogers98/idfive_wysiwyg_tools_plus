A way to add custom buttons/snippets to WYSIWYG module (using CKeditor lib).
- Kept some custom slideshow snippet in (relies on https://www.drupal.org/project/insert_block) to leave an idea of whats possible.

Editing
- Hack code to bits, as needed. Commit back if worthwhile.
  - idfive_wysiwyg_tools_plus.module Custom FAES block/view/shortcode stuff starts line 29, you'll likely want to edit/delete/customize, as it wont work without the corresponding view installed!!
  - css/idfive.css site wide css to do with module stuff. Renders on client view, sitewide.
  - plugins/tools.inc is where you will define standard btn's
  - Add js to correspond to new snippet (ie my_snippet.js), see examples
  - plugins/wysiwyg_view.css is css for while you are "in the editor", so add some visuals to help users edit what the are doing within wysiwyg.
  - plugins/icons your img folder for icons to appear in the wysiwyg tool bar

Install
- Install as per normal
- Edit desired WYSIWYG profile (ie, full html), and enable the snippets you add.
- Drink coffee, air five Dan, because he just saved you some work.
